<?php

namespace Drupal\jsf_graphql_mutations\Plugin\GraphQL\InputTypes;

use Drupal\graphql\Plugin\GraphQL\InputTypes\InputTypePluginBase;

/**
 * The input type for basic page mutations.
 *
 * @GraphQLInputType(
 *   id = "user_input",
 *   name = "UserInput",
 *   fields = {
 *     "mail" = "String",
 *     "name" = "String",
 *     "password" = "String",
 *   }
 * )
 */
class UserInput extends InputTypePluginBase {

}
