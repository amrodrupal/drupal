<?php

namespace Drupal\bp_settings\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Always deny access to the following routes.
    $not_accessible_routes = [
      'filter.tips_all',
      'filter.tips'
    ];
    foreach ($not_accessible_routes as $name) {
      if ($route = $collection->get($name)) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
