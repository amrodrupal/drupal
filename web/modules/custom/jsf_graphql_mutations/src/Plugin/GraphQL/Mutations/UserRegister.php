<?php

namespace Drupal\jsf_graphql_mutations\Plugin\GraphQL\Mutations;

use Drupal\graphql\Annotation\GraphQLMutation;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_core\Plugin\GraphQL\Mutations\Entity\CreateEntityBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Simple mutation for creating a new article node.
 *
 * @GraphQLMutation(
 *   id = "user_register",
 *   entity_type = "user",
 *   entity_bundle = "user",
 *   secure = true,
 *   name = "UserRegister",
 *   type = "EntityCrudOutput!",
 *   arguments = {
 *     "input" = "UserInput"
 *   }
 * )
 */
class UserRegister extends CreateEntityBase {

  /**
   * {@inheritdoc}
   */
  protected function extractEntityInput(
    $value,
    array $args,
    ResolveContext $context,
    ResolveInfo $info
  ) {
    return [
      'name' => $args['input']['name'],
      'mail' => $args['input']['mail'],
      'pass' => $args['input']['password'],
      'status' => 1
    ];
  }

}
