<?php

namespace Drupal\jsf_graphql_mutations\Plugin\GraphQL\InputTypes;

use Drupal\graphql\Plugin\GraphQL\InputTypes\InputTypePluginBase;

/**
 * The input type for basic page mutations.
 *
 * @GraphQLInputType(
 *   id = "page_input",
 *   name = "PageInput",
 *   fields = {
 *     "title" = "String",
 *     "field_ct_teaser_text" = {
 *        "type" = "String",
 *        "nullable" = "TRUE"
 *     }
 *   }
 * )
 */
class PageInput extends InputTypePluginBase {

}