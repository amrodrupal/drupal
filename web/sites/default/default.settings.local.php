<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Local development override configuration feature.
 */

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

/**
 * Load any .env file. See /.env.example.
 */
$dotenv = new Dotenv(__DIR__);
try {
  $dotenv->load();
}
catch (InvalidPathException $e) {
  // Do nothing. Production environments rarely use .env files.
}

/**
 * Assertions.
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

/**
 * Database settings.
 */
$databases['default']['default'] = [
  'database' => getenv('BP_DB_DATABASE'),
  'username' => getenv('BP_DB_USERNAME'),
  'password' => getenv('BP_DB_PASSWORD'),
  'prefix' => '',
  'host' => getenv('DB_HOST'),
  'port' => 3306,
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

/**
 * Drupal settings.
 */
$settings['config_sync_directory'] = '../config/default/drupal/sync';
$settings['file_private_path'] = getenv('BP_FILE_PRIVATE_PATH');
$settings['trusted_host_patterns'] = [
  getenv('BP_SITE_URL'),
];

/**
 * Show all error messages, with backtrace information.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
//$config['system.performance']['css']['preprocess'] = FALSE;
//$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Services YAML.
 */
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Caching for development purposes.
 */
//$settings['cache']['bins']['render'] = 'cache.backend.null';
//$settings['cache']['bins']['page'] = 'cache.backend.null';
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

/**
 * Exclude modules from configuration synchronisation.
 */
# $settings['config_exclude_modules'] = ['devel', 'stage_file_proxy'];
