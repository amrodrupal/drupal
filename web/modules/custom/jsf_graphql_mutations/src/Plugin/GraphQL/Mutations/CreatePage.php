<?php

namespace Drupal\jsf_graphql_mutations\Plugin\GraphQL\Mutations;

use Drupal\graphql\Annotation\GraphQLMutation;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_core\Plugin\GraphQL\Mutations\Entity\CreateEntityBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Simple mutation for creating a new article node.
 *
 * @GraphQLMutation(
 *   id = "create_page",
 *   entity_type = "node",
 *   entity_bundle = "ct_basic_page",
 *   secure = true,
 *   name = "createPage",
 *   type = "EntityCrudOutput!",
 *   arguments = {
 *     "input" = "PageInput"
 *   }
 * )
 */
class CreatePage extends CreateEntityBase {

  /**
   * {@inheritdoc}
   */
  protected function extractEntityInput(
    $value,
    array $args,
    ResolveContext $context,
    ResolveInfo $info
  ) {
    return [
      'title' => $args['input']['title'],
      'field_ct_teaser_text' => $args['input']['field_ct_teaser_text'],
      'status' => 1
    ];
  }

}
