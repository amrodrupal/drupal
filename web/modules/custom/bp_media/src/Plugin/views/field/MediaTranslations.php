<?php

namespace Drupal\bp_media\Plugin\views\field;

use Drupal\media\Entity\Media;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Field handler to show available media entity translations.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("bp_media_translations")
 */
class MediaTranslations extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    if (empty($values->mid)) {
      return FALSE;
    }

    $media_entity = Media::load($values->mid);
    $available_languages = $media_entity->getTranslationLanguages();
    if (empty($available_languages)) {
      return FALSE;
    }
    $translations = [];
    foreach ($available_languages as $langcode => $language) {
      $url = Url::fromRoute(
        'entity.media.edit_form',
        ['media' => $media_entity->id()],
        ['language' => $language]
      );
      $link = Link::fromTextAndUrl(strtoupper($langcode), $url);
      $translations[] = $link->toRenderable();
    }

    $links = [
      '#items' => $translations,
      '#theme' => 'item_list',
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Available translations');
  }

}
